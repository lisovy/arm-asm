/* FIXME
 * Use volatile
 */
#define mem_write_32(dest, val)	\
	*((uint32_t*)(dest)) = val;
#define mem_read_32(src) \
	*((uint32_t*)(src))

#define GPIO3_IO28_PINMUX	0x020E00C4
#define GPIO3_IO28_DIR		0x020A4004
#define GPIO3_IO28_ADDR		0x020A4000
#define GPIO3_IO28_DATA		0x10000000

#define uint32_t 		int
#define TRUE			1


/* Global variable -- global scope */
uint32_t val;

void delay(void)
{
	/* Local variable -- valid only when executing in function */
	unsigned int i = 100000000;

	while (i > 0)
		i--;	/* Decrement */
}

/* Entry point */
void main(void)
{
	mem_write_32(GPIO3_IO28_PINMUX, 0x5);
	mem_write_32(GPIO3_IO28_DIR, GPIO3_IO28_DATA);

	/* Execute forever */
	while (TRUE) {
		/* Store data present on "GPIO3_IO28_ADDR" memory location
		   to the memory location reserved for "val" variable */
		val = mem_read_32(GPIO3_IO28_ADDR);

		val = val ^ GPIO3_IO28_DATA;
		mem_write_32(GPIO3_IO28_ADDR, val);

		/* Call procedure ... */
		delay();
		/* ... and return back from it */

		/* Wrap around and execute the while cycle if the
		   "condition" is true */
	}
}
